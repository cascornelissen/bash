#!/usr/bin/env bash

VERBOSE_COLOR='\033[1;30m'
DEBUG_COLOR='\033[1;30m'
INFO_COLOR='\033[0;34m'
WARNING_COLOR='\033[1;33m'
ERROR_COLOR='\033[0;31m'
NO_COLOR='\033[0m'

function log_verbose() {
    echo -e "${VERBOSE_COLOR}VRB | $*${NO_COLOR}"
}

function log_debug() {
    echo -e "${DEBUG_COLOR}DBG${NO_COLOR} | $*"
}

function log_info() {
    echo -e "${INFO_COLOR}NFO${NO_COLOR} | $*"
}

function log_warning() {
    echo -e "${WARNING_COLOR}WRN${NO_COLOR} | $*"
}

function log_error() {
    echo -e "${ERROR_COLOR}ERR${NO_COLOR} | $*"
}
