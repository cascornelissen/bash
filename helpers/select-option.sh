#!/usr/bin/env bash

# https://unix.stackexchange.com/a/415155/290789
function select_option {
    ESC=$(printf "\033")
    local -n RESULT="${1}"
    local QUESTION="\n${2}\n"
    local -n OPTIONS=$3
    local IS_MULTI=$4
    local CURRENT_OPTION=0
    local MULTI_VALUE=("${OPTIONS[@]}")

    cursor_blink_off() {
        printf "${ESC}[?25l";
    }

    cursor_blink_on() {
        printf "${ESC}[?25h";
    }

    cursor_to() {
        printf "${ESC}[${1};${2:-1}H";
    }

    clear_line() {
        printf "${ESC}[K";
    }

    print_option_prefix() {
        if [ "$IS_MULTI" = true ]; then
            if [[ " ${MULTI_VALUE[*]} " == *" ${1} "* ]]; then
                printf "[✓] "
            else
                printf "[ ] "
            fi
        fi
    }

    print_option() {
        printf "${ESC}[K${ESC}[90m    $(print_option_prefix "${1}")%s${ESC}[39m" "${1}";
    }

    print_option_current() {
        printf "${ESC}[K${ESC}[0m  → $(print_option_prefix "${1}")%s" "${1}";
    }

    print_more_up() {
        printf "${ESC}[K${ESC}[90m  ↑ %s more options" "${1}";
    }

    print_more_down() {
        printf "${ESC}[K${ESC}[90m  ↓ %s more options" "${1}";
    }

    get_cursor_row() {
        # https://stackoverflow.com/a/2575525
        exec < /dev/tty
        OLD_STTY=$(stty -g)
        stty raw -echo min 0
        echo -en "\033[6n" > /dev/tty
        IFS=';' read -r -d R -a POSITION
        stty $OLD_STTY

        echo "${POSITION[0]:2}"
    }

    get_max_rows() {
        echo $(("$(tput lines)" - "$(printf '%s' "${PS1@P}" | wc -l)" - $(echo -e "${QUESTION}" | wc -l) - 3));
    }

    key_input() {
        IFS=;
        local key

        read -rsn 100 -t 0.001

        while true; do
            read -rsn 1 # Read first byte of key
            key=$REPLY

            if [[ "$key" == $'' ]]; then echo "enter"; break; fi
            if [[ "$key" == ' ' ]]; then echo "space"; break; fi
            if [[ "$key" == "${ESC}" ]]; then
                # Read 2 more bytes if it's an escape sequence, or nothing if it's just Esc.
                read -rsn 2 -t 0.001

                [[ "$REPLY" != "" ]] && key+="$REPLY"
                if [[ "$key" == "${ESC}[A" ]]; then echo "up"; break; fi
                if [[ "$key" == "${ESC}[B" ]]; then echo "down"; break; fi
                if [[ "$key" == "${ESC}[5" ]]; then echo "pageup"; break; fi
                if [[ "$key" == "${ESC}[6" ]]; then echo "pagedown"; break; fi
                if [[ "$key" == "${ESC}[H" ]]; then echo "home"; break; fi
                if [[ "$key" == "${ESC}[F" ]]; then echo "end"; break; fi
                if [[ "$key" == "${ESC}"   ]]; then echo "esc"; break; fi

                key=;
            fi
        done
    }

    # Question is the first argument, the second arguments should be passed-by-reference array
    echo -e "${QUESTION}"

    # Initially print empty new lines (scroll down if at bottom of screen)
    local MAX_ROWS=$(get_max_rows)
    for i in "${!OPTIONS[@]}"; do
        if [ "${i}" -lt "$MAX_ROWS" ]; then
            printf "\n";
        fi
    done

    # Determine current screen position for overwriting the options
    local LAST_ROW=$(get_cursor_row)
    if [ "${#OPTIONS[@]}" -lt "$MAX_ROWS" ]; then
        local startrow=$(("$LAST_ROW" - "${#OPTIONS[@]}"))
    else
        local startrow=$(("$LAST_ROW" - "$MAX_ROWS"))
    fi

    # Ensure cursor and input echoing back on upon a ctrl+c during read -s
    trap "cursor_blink_on; stty echo; printf '\n'; exit" 2
    cursor_blink_off

    local more_options_height=2
    local offset=0
    while true; do
        # Print options by overwriting the last lines
        local idx=0

        if [ $offset -gt $(("${#OPTIONS[@]}" - "$MAX_ROWS")) ]; then
            offset=$(("${#OPTIONS[@]}" - "$MAX_ROWS"))
        fi

        if [ $offset -lt 0 ]; then
            offset=0
        fi

        local more_options_down=$(("${#OPTIONS[@]}" - ("$MAX_ROWS" + "$offset") + "$more_options_height"))
        local more_options_down_height=$([ "$more_options_down" -gt 2 ] && echo $more_options_height || echo 0)

        for i in "${!OPTIONS[@]}"; do
            if [ "${i}" -ge "${offset}" ] && [ "${i}" -lt $(("$MAX_ROWS" + "$offset" - "$more_options_down_height")) ]; then
                cursor_to $(("$startrow" + "$idx"))

                if [ $idx -eq $(("$CURRENT_OPTION" - "$offset")) ]; then
                    print_option_current "${OPTIONS[$i]}"
                else
                    print_option "${OPTIONS[$i]}"
                fi

                ((idx++))
            fi
        done

        if [ $more_options_down -gt $more_options_height ]; then
            cursor_to $(("$LAST_ROW" - 2))
            clear_line
            cursor_to $(("$LAST_ROW" - 1))
            print_more_down $more_options_down
        fi

        # User key control
        case $(key_input) in
            enter)    break;;
            space)    if [[ " ${MULTI_VALUE[*]} " == *" ${OPTIONS[CURRENT_OPTION]} "* ]]; then
                          for i in "${!MULTI_VALUE[@]}"; do
                              if [[ ${MULTI_VALUE[i]} = "${OPTIONS[CURRENT_OPTION]}" ]]; then
                                  unset 'MULTI_VALUE[i]'
                              fi
                          done
                      else
                          MULTI_VALUE+=("${OPTIONS[CURRENT_OPTION]}")
                      fi
                      ;;
            up)       ((CURRENT_OPTION--));
                      if [ "${CURRENT_OPTION}" -eq $(("$offset" - 1)) ]; then
                          ((offset--));
                      fi;
                      if [ "${CURRENT_OPTION}" -lt 0 ]; then
                          CURRENT_OPTION=$(("${#OPTIONS[@]}" - 1));
                          offset=$(("${#OPTIONS[@]}" - "$MAX_ROWS"))
                      fi
                      ;;
            down)     ((CURRENT_OPTION++));
                      if [ "${CURRENT_OPTION}" -eq $(("$MAX_ROWS" + "$offset" - "$more_options_down_height")) ]; then
                          ((offset++));
                      fi;
                      if [ "${CURRENT_OPTION}" -ge "${#OPTIONS[@]}" ]; then
                          CURRENT_OPTION=0;
                          offset=0;
                      fi
                      ;;
            pageup)   ((CURRENT_OPTION-=10));
                      if [ "${CURRENT_OPTION}" -le $offset ]; then
                          ((offset-=10))

                          if [ "$more_options_down_height" -eq 0 ]; then
                              ((offset+="$more_options_height"))
                          fi
                      fi
                      if [ "${CURRENT_OPTION}" -lt 0 ]; then
                          CURRENT_OPTION=0;
                          offset=0;
                      fi
                      ;;
            pagedown) ((CURRENT_OPTION+=10));
                      if [ "${CURRENT_OPTION}" -ge $(("$MAX_ROWS" + "$offset" - "$more_options_down_height")) ]; then
                          ((offset+=10));
                      fi;
                      if [ "${CURRENT_OPTION}" -ge "${#OPTIONS[@]}" ]; then
                          CURRENT_OPTION=$(("${#OPTIONS[@]}" - 1));
                          offset=$(("${#OPTIONS[@]}" - "$MAX_ROWS"))
                      fi
                      ;;
            home)     CURRENT_OPTION=0;
                      offset=0
                      ;;
            end)      CURRENT_OPTION=$(("${#OPTIONS[@]}" - 1));
                      offset=$(("${#OPTIONS[@]}" - "$MAX_ROWS"))
                      ;;
        esac
    done

    # Cursor position back to normal
    cursor_to "$LAST_ROW"
    printf "\n"
    cursor_blink_on

    if [ "$IS_MULTI" = true ]; then
        RESULT=("${MULTI_VALUE[@]}")
    else
        RESULT="${OPTIONS[${CURRENT_OPTION}]}"
    fi
}
