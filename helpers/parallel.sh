function parallel() {
    local -n RESULTS=$1
    local -n COMMANDS=$2
    local TMP_MAKEFILE="/tmp/bash-makefile-$(date +%s)"

    # Build Makefile
    printf "all:" >> "${TMP_MAKEFILE}"

    for i in "${!COMMANDS[@]}"; do
        TARGET="command${i}"

        printf " ${TARGET}" >> "${TMP_MAKEFILE}"
    done

    printf "\n\n" >> "${TMP_MAKEFILE}"

    for i in "${!COMMANDS[@]}"; do
        TARGET="command${i}"
        TMP_OUT_FILE="${TMP_MAKEFILE}-${i}"

        printf ".PHONY: ${TARGET}\n${TARGET}:\n\t${COMMANDS[$i]} >> ${TMP_OUT_FILE}\n\n" >> "${TMP_MAKEFILE}"
    done

    # Execute Makefile
    make --makefile "${TMP_MAKEFILE}" --silent --jobs --output-sync

    for i in "${!COMMANDS[@]}"; do
        TMP_OUT_FILE="${TMP_MAKEFILE}-${i}"
        RESULTS+=($(cat "${TMP_OUT_FILE}"))

        rm "${TMP_OUT_FILE}"
    done

    rm "${TMP_MAKEFILE}"
}
