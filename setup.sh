#!/usr/bin/env bash

DIRECTORY=$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd -P)

source "${DIRECTORY}/variables.sh"
source "${DIRECTORY}/helpers/log.sh"
source "${DIRECTORY}/helpers/select-option.sh"

if [ ! -f "${PROFILE_FILE}" ]; then
    touch "${PROFILE_FILE}"
fi

INSTALL_DIR="$(eval echo "~/.bash")"
LINE="source \"${INSTALL_DIR}/aliases.sh\""
CONTENT="${LINE}\n\n$(cat "${PROFILE_FILE}")"

options=(Yes No)
select_option answer "Do you want to modify the prompt (PS1)?" options

if [ "${answer}" == "Yes" ]; then
    read -r -p "What username would you like to display in the prompt (PS1)? [${USER}]: " PS1_USER
    PS1_USER=${PS1_USER:-${USER}}
    read -r -p "What hostname would you like to display in the prompt (PS1)? [$(hostname)]: " PS1_HOST
    PS1_HOST=${PS1_HOST:-$(hostname)}

    read -r -p "Where are you development projects located? [${HOME}/projects]: " WORKING_DIRECTORY
    WORKING_DIRECTORY=${WORKING_DIRECTORY:-${HOME}/projects}

    cat << EOF > "${INSTALL_DIR}/config.sh"
export PS1_USER="${PS1_USER}"
export PS1_HOST="${PS1_HOST}"
export WORKING_DIRECTORY="${WORKING_DIRECTORY}"
EOF

    source "${PROFILE_FILE}"
fi

if grep -Fxq "${LINE}" "${PROFILE_FILE}"; then
    log_debug "Scripts are installed in ${PROFILE_FILE} already"
else
    echo -e "${CONTENT}" > "${PROFILE_FILE}"
fi

readarray -t options < <(grep '^alias' "${DIRECTORY}/setup/aliases.sh" | grep ' # ' | awk -F "[=#]" '{print substr($1, 7) " \033[1;30m→"$(NF)"\033[0m"}')
select_option aliases "Which shell aliases would you like to be available?" options true

cp "${DIRECTORY}/setup/aliases.sh" "${DIRECTORY}/aliases.sh"
sed -i '' 's/^alias/# alias/' "${DIRECTORY}/aliases.sh"

for ALIAS in "${aliases[@]}"; do
    alias="$(echo "${ALIAS}" | cut -d ' ' -f 1)"

    sed -i '' "s/^# alias ${alias}/alias ${alias}/" "${DIRECTORY}/aliases.sh"
done

if [[ "${OSTYPE}" == "darwin"* ]]; then
    # Brew
    if ! [ -x "$(command -v brew)" ]; then
        options=(Yes No)
        select_option answer "Do you want to install brew (https://brew.sh) to manage system packages and applications?" options

        if [ "${answer}" == "Yes" ]; then
            /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
        fi
    fi

    # Packages
    if [ -x "$(command -v brew)" ]; then
        options=(bash node git tmux tmuxinator kubernetes-cli kubectx htop curl dockutil jq make awscli awsume jordanbaird-ice)
        select_option packages "Which system packages would you like to install?" options true

        if ! [ ${#packages[@]} -eq 0 ]; then
            brew install "${packages[@]}"
        fi

        options=(Yes No)
        select_option answer "Would you like GNU versions of installed system packages to replace OS versions?" options

        if [ "${answer}" == "Yes" ]; then
            sudo mkdir -p /usr/local/gnubin

            for GNU_UTIL in /usr/local/opt/**/libexec/gnubin/*; do
                sudo ln -s "${GNU_UTIL}" /usr/local/gnubin
            done

            printf "\nPATH=\"/opt/homebrew/opt/make/libexec/gnubin:\$PATH\"" >> "${PROFILE_FILE}"
        fi

        options=(google-chrome firefox opera iterm2 phpstorm datagrip 1password 1password-cli slack microsoft-teams microsoft-outlook adoptopenjdk docker spotify alfred sensiblesidebuttons)
        select_option packages "Which applications would you like to install?" options true

        if ! [ ${#packages[@]} -eq 0 ]; then
            brew install --cask "${packages[@]}"
        fi
    fi

    # Set default bash shell to the one that was just installed
    # for both the current user and the root user
    options=(Yes No)
    select_option answer "Do you want to set the default shell for users $(whoami) and root to bash?" options

    if [ "${answer}" == "Yes" ]; then
        echo "/usr/local/bin/bash" | sudo tee -a /etc/shells
        chsh -s /usr/local/bin/bash
        sudo chsh -s /usr/local/bin/bash
    fi

    # Add global .gitignore
    options=(Yes No)
    select_option answer "Do you want set up a global .gitignore with the following content?\n$(cat "${DIRECTORY}/setup/files/.gitignore")" options

    if [ "${answer}" == "Yes" ]; then
        cp "${DIRECTORY}/setup/files/.gitignore" ~/.gitignore
        git config --global core.excludesFile ~/.gitignore
    fi

    # Configure nano
    options=(Yes No)
    select_option answer "Do you want set up a nano configuration with the following content?\n$(cat "${DIRECTORY}/setup/files/.nanorc")" options

    if [ "${answer}" == "Yes" ]; then
        cp "${DIRECTORY}/setup/files/.nanorc" ~/.nanorc
    fi

    # Configure tmux
    options=(Yes No)
    select_option answer "Do you want set up a tmux configuration with the following content?\n$(cat "${DIRECTORY}/setup/files/.tmux.conf")" options

    if [ "${answer}" == "Yes" ]; then
        cp "${DIRECTORY}/setup/files/.tmux.conf" ~/.tmux.conf
    fi

    # Organize dock
    options=(Yes No)
    select_option answer "Do you want to organize the dock?" options

    if [ "${answer}" == "Yes" ]; then
        dockutil --remove all
        dockutil --add "/Applications/Firefox.app"
        dockutil --add "/Applications/Google Chrome.app"
        dockutil --add "/Applications/Safari.app"
        dockutil --add "/Applications/PhpStorm.app"
        dockutil --add "/Applications/DataGrip.app"
        dockutil --add "/Applications/iTerm.app"
        dockutil --add "/Applications/Slack.app"
        dockutil --add "/Applications/Microsoft Teams.app"
        dockutil --add "/Applications/Microsoft Outlook.app"
        dockutil --add "/Applications/1Password 7.app"
    fi

    source "${PROFILE_FILE}"
else
    log_error "Unsupported OS '${OSTYPE}'"
    exit 1
fi

options=(Yes No)
select_option answer "Do you to automatically check for updates to this setup?" options

if [ "${answer}" == "No" ]; then
    sed -i '' '/update-check.sh/s/^/# /' "${DIRECTORY}/aliases.sh"
fi

log_info "Done, everything is set up matching your provided preferences"
