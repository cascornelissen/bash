#!/usr/bin/env bash

source "${BASH_SOURCE%/*}/../helpers/log.sh"

ACTION="${1}"
BRANCH="$(git branch --show-current)"
REMOTE="$(git ls-remote --get-url origin)"

if [[ "${REMOTE}" =~ git-ssh.devops.wartsila.com ]]; then
    PROJECT="$(echo "${REMOTE}" | sed -E 's/.*\/(.*)\/.*/\1/')"
    REPOSITORY="$(echo "${REMOTE}" | sed -E 's/.*\/(.*)\.git/\1/')"
    LINK="https://git.devops.wartsila.com/projects/${PROJECT}/repos/${REPOSITORY}"

    case $ACTION in
        '')
            ;;
        pr)
            LINK="${LINK}/pull-requests?create&sourceBranch=refs/heads/${BRANCH}"
            ;;
        prs)
            LINK="${LINK}/pull-requests"
            ;;
        branches)
            LINK="${LINK}/branches"
            ;;
        commits)
            LINK="${LINK}/commits"
            ;;
        builds)
            LINK="${LINK}/builds"
            ;;
        *)
            log_error "Unsupported action: ${ACTION}"
            exit 1
            ;;
    esac
else
    log_error "Unsupported remote: ${REMOTE}."
    exit 1
fi

open "${LINK}"
