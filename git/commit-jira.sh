#!/usr/bin/env bash

source "${BASH_SOURCE%/*}/../helpers/log.sh"

BRANCH=$(git rev-parse --abbrev-ref HEAD)
STRIPPED_BRANCH=$(echo "${BRANCH}" | sed 's/.*\///')

read -r -a PARTS <<< "${STRIPPED_BRANCH//-/ }"

if ! [[ "${PARTS[1]}" =~ ^[0-9]+$ ]]; then
    log_error "Not on a jira branch"
    exit
fi

git commit -m "${PARTS[0]}-${PARTS[1]} ${1}" ${@:2}
