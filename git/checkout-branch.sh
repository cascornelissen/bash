#!/usr/bin/env bash

source "${BASH_SOURCE%/*}/../helpers/log.sh"
source "${BASH_SOURCE%/*}/../helpers/select-option.sh"

git fetch --all --quiet
mapfile -t remotes < <(git remote)
mapfile -t options < <(git for-each-ref --format='%(refname:short)' refs/heads/ refs/remotes/ | grep -v "HEAD" | grep "${1}" | sort)

# Strip remote from the options and append it visually
for i in "${!options[@]}"; do
    for remote in ${remotes[*]}; do
        if [[ "${options[$i]}" == "${remote}/"* ]]; then
            options[$i]="${options[$i]//${remote}\/} [remotes/${remote}]"
        fi
    done
done

if [ "${#options[@]}" -eq 0 ]; then
    log_error "No branches matching '${1}' found"
elif [ "${#options[@]}" -eq 1 ]; then
    git checkout "$(echo "${options[0]}" | cut -d' ' -f1)"
else
    select_option answer "Which branch would you like to check out?" options

    git checkout "$(echo "${answer}" | cut -d' ' -f1)"
fi
