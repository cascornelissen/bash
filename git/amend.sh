#!/usr/bin/env bash

source "${BASH_SOURCE%/*}/../helpers/log.sh"

if [ "$(test -d "$(git rev-parse --git-path rebase-merge)" || test -d "$(git rev-parse --git-path rebase-apply)")" ]; then
    log_error "Rebase in progress, aborting amend"
    exit
fi

if [ "$(git diff --cached --quiet)" ]; then
    log_warn "No staged changes found"
fi

git commit --amend --no-edit "$@"
