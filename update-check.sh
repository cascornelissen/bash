#!/usr/bin/env bash

source "${BASH_SOURCE%/*}/variables.sh"

# Fetch the latest log in the background
git -C "${BASH_SOURCE%/*}" fetch &>/dev/null & disown
git -C "${BASH_SOURCE%/*}" log --oneline --decorate HEAD..origin/master 1> "${UPDATE_LOG_FILE}" & disown

if [ -f "${UPDATE_LOG_FILE}" ] && [ -s "${UPDATE_LOG_FILE}" ]; then
    echo ""
    echo "There are new commits on the bash repository, run the following command if you'd like to update"
    echo "source ${BASH_SOURCE%/*}/update.sh"
    echo ""
fi
