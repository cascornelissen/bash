#!/usr/bin/env bash

export PROFILE_FILE="$(eval echo "~/.bash_profile")"
export UPDATE_LOG_FILE="${BASH_SOURCE%/*}/.update.log"
