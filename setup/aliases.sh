#!/usr/bin/env bash

source "${BASH_SOURCE%/*}/config.sh"
source "${BASH_SOURCE%/*}/update-check.sh"

# Bash
alias ll="${BASH_SOURCE%/*}/miscellaneous/ls.sh" # Improved, more verbose ls

# Tmux
alias t="tmux" # Shortcut for tmux
alias ta="tmux attach" # Shortcut for tmux attach
alias tls="tmux list-windows" # Lists available tmux windows
alias s="${BASH_SOURCE%/*}/tmux/start-project.sh" # Starts a tmux project using tmuxinator

# Git
alias gs="git status" # Shortcut for git status
alias gl="git log --oneline --all --graph --decorate" # Improved, more verbose git log
alias gaa="git add -A :/ && git status" # Adds all files in current repository and logs status
alias gc="git commit --message ${@}" # Creates a git commit with the provided message
alias gca="git commit --amend --no-edit" # Amends the staged changes without editing the commit
alias gcj="${BASH_SOURCE%/*}/git/commit-jira.sh" # Creates a git commit with the Jira ticket number prefixed
alias gpb="${BASH_SOURCE%/*}/git/push-branch.sh" # Pushes the current branch to a branch on origin with the same name
alias gd="git diff -- :/ ':(exclude)*package-lock.json' ':(exclude)*composer.lock'" # Improved git diff for unstaged files, excludes lockfiles
alias gds="git diff --staged -- :/ ':(exclude)*package-lock.json' ':(exclude)*composer.lock'" # Improved git diff for staged files, excludes lockfiles
alias gcb="${BASH_SOURCE%/*}/git/checkout-branch.sh" # Finds and checks out a branch based on the provided pattern
alias grc="GIT_EDITOR=true git rebase --continue" # Continue git rebase without editing the commit message
alias repo="${BASH_SOURCE%/*}/git/repo.sh" # Opens the relevant page for the current repository in the browser

# Docker
alias d="docker" # Shortcut for docker
alias dc="docker-compose" # Shortcut for docker-compose
alias dps="docker ps --format \"table {{.Names}}\t{{.Status}}\t{{.Image}}\"" # Minimal version of docker ps
alias dstopall='docker stop $(docker ps -q)' # Stops all running containers
alias dnorestartall='docker update --restart=no $(docker ps -a -q)' # Updates the restart flag for all containers to 'no'
alias dcon="${BASH_SOURCE%/*}/docker/connect.sh" # Connects to a running container based on the provided pattern

# Kubernetes
alias k="kubectl" # Shortcut for kubectl
alias kpf="${BASH_SOURCE%/*}/kubernetes/port-forward.sh" # Kubernetes port-forward with support for multiple port-forwards and retry mechanism

# NPM
alias npmrelock="npm install --package-lock-only --ignore-scripts" # Regenerates the lockfile without installing dependencies, useful for resolving conflicts

# Miscellaneous
alias syncmd="${BASH_SOURCE%/*}/miscellaneous/syncmd.sh" # Starts a tmux session with synchronized panes for the matching directories
alias freeport="${BASH_SOURCE%/*}/miscellaneous/free-port.sh" # Frees up the specified port
alias flushdns="${BASH_SOURCE%/*}/miscellaneous/flush-dns.sh" # Flushes DNS cache
alias code="${BASH_SOURCE%/*}/miscellaneous/code-editor.sh" # Opens WebStorm
alias coverage="${BASH_SOURCE%/*}/miscellaneous/coverage.sh" # Opens the coverage report in the browser
alias e="${BASH_SOURCE%/*}/miscellaneous/explorer.sh" # Opens Finder or Windows Explorer

# Colors
export CLICOLOR=1
export LS_COLORS="di=38;5;33:ln=35:so=32:pi=33:ex=31:bd=34;46:cd=34;43:su=30;41:sg=30;46:tw=30;42:ow=38;5;33"

# Functions
function title() {
    echo -ne "\033]0;"$*"\007"
}

function git_branch() {
    if git rev-parse --git-dir > /dev/null 2>&1; then
        if ls "$(git rev-parse --git-dir)" | grep rebase > /dev/null 2>&1; then
            echo -e "\033[00;31m[rebasing]\033[0m"
        else
            echo -e "\033[00;33m$(git branch --show-current 2> /dev/null)\033[0m"
        fi
    fi
}

function git_last_commit_message() {
    echo -e "\033[01;30m$(git log -1 --pretty=%B 2> /dev/null | head -n 1)\033[0m"
}

# PS1
[ -z "${PS1_USER}" ] && PS1_USER="\u" || PS1_USER="${PS1_USER}"
[ -z "${PS1_HOST}" ] && PS1_HOST="\H" || PS1_HOST="${PS1_HOST}"
export PS1="\[\033[01;30m\]\n[\t] \[\033[00;37m\]${PS1_USER}\[\033[01;30m\]@\[\033[00;37m\]${PS1_HOST} \[\033[00;32m\]\w \$(git_branch) \$(git_last_commit_message) \[\033[00;37m\] \n\$ \[\033[00m\]"
