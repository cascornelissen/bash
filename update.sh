#!/usr/bin/env bash

source "${BASH_SOURCE%/*}/variables.sh"

echo "Fetching commits from upstream..."
git -C "${BASH_SOURCE%/*}" fetch

LOG=$(git -C "${BASH_SOURCE%/*}" log --oneline --decorate HEAD..origin/master)

if [ -n "${LOG}" ]; then
    echo ""
    echo "${LOG}"
    echo ""

    read -r -p "Do you want to pull in the updates listed above? [Y/n] " response
    if [[ "${response}" =~ ^(yes|y| ) ]] || [[ -z "${response}" ]]; then
        git -C "${BASH_SOURCE%/*}" pull --ff-only
        rm -f "${UPDATE_LOG_FILE}"
        source "${PROFILE_FILE}"
    fi
else
    echo "This setup is up-to-date"
fi
