#!/usr/bin/env bash

source "${BASH_SOURCE%/*}/../helpers/log.sh"

if [ -z "${1}" ]; then
    log_error "No port specified"
    exit
fi

PID=$(lsof -ti tcp:"${1}")

if [ -z "${PID}" ]; then
    log_error "No process running on TCP port ${1}"
else
    kill "${PID}"
    log_info "Process using TCP port ${1} was stopped successfully"
fi
