#!/usr/bin/env bash

source "${BASH_SOURCE%/*}/../helpers/log.sh"

set -e

if [[ "${OSTYPE}" == "darwin"* ]]; then
    # macOS
    "${BASH_SOURCE%/*}/sudo.sh" "${0}" "${@}"

    sudo dscacheutil -flushcache
    sudo killall -HUP mDNSResponder

    log_info "DNS cache flushed successfully"
else
    log_error "Unsupported OS '${OSTYPE}'"
    exit 1
fi
