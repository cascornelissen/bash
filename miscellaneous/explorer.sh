#!/usr/bin/env bash

source "${BASH_SOURCE%/*}/../helpers/log.sh"

if uname -a | grep -qi '^Linux.*Microsoft'; then
    # WSL
    if [ -n "${1}" ]; then
        DIR=$(wslpath -w "${1}")
    else
        DIR=$(wslpath -w "$(pwd -P)")
    fi

    explorer.exe "${DIR}"
elif [[ "${OSTYPE}" == "darwin"* ]]; then
    # macOS
    if [ -n "${1}" ]; then
        DIR="${1}"
    else
        DIR=$(pwd -P)
    fi

    open "${DIR}"
else
    log_error "Unsupported OS '${OSTYPE}'"
    exit 1
fi
