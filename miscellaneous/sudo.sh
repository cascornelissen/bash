#!/usr/bin/env bash

source "${BASH_SOURCE%/*}/../helpers/log.sh"

if [ "${UID}" -ne 0 ]; then
    log_warning "System administrator rights are required to execute this script."
    exec sudo "${0}" "${@}"
    exit
fi
