#!/usr/bin/env bash

source "${BASH_SOURCE%/*}/../helpers/log.sh"

if uname -a | grep -qi '^Linux.*Microsoft'; then
    # WSL
    EDITOR=$(find '/mnt/c/Users/Cas/AppData/Local/Programs/WebStorm' -maxdepth 4 -iname 'webstorm64.exe' | tail -1)

    if [ -z "${EDITOR}" ]; then
        log_error 'WebStorm could not be found'
        exit 1
    fi

    # Transform linux path to Windows path in WSL
    DIRECTORY="$(pwd -P)"

    if [ -n "${1}" ]; then
        DIRECTORY="${1}"
    fi

    "${EDITOR}" "$(wslpath -w "${DIRECTORY}"| sed 's/wsl.localhost/wsl$/')" 2>/dev/null 1>&2 &
elif [[ "${OSTYPE}" == "darwin"* ]]; then
    # macOS
    EDITOR=$(find '/Applications' "${HOME}/Applications" -iname 'WebStorm.app' -maxdepth 1 | head -n 1)

    if [ -z "$EDITOR" ]; then
        log_error 'WebStorm could not be found'
        exit 1
    fi

    open -a "${EDITOR}" "$@"
else
    log_error "Unsupported OS '${OSTYPE}'"
    exit 1
fi
