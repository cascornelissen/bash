#!/usr/bin/env bash

if uname -a | grep -qi '^Linux.*Microsoft'; then
    # WSL
    ls -lashp --color=auto
elif [[ "${OSTYPE}" == "darwin"* ]]; then
    # macOS
    ls -lashpc
else
    ls -lashp
fi
