#!/usr/bin/env bash

# $ syncmd
#   Provides a list of all directories in the current working directory
#
# $ syncmd ui
#   Provides a list of all directories containing "ui" in the current working directory
#
# $ syncmd "$(find . -type f -path '*/bamboo-specs/bamboo.yaml' -maxdepth 3 -print0 | xargs -0 grep -l 'TERM' | awk -F "/" '{print $2}')"
# $ syncmd "$(find . -type d -name 'node_modules' -prune -o -type d -name '.git' -prune -o -type f \( -name '*.ts' -o -name '*.tsx' \) -exec grep -l 'TERM' {} \; | awk -F "/" '{print $2}' | uniq)"
#   Provides a list of directories returned by find

source "${BASH_SOURCE%/*}/../helpers/log.sh"
source "${BASH_SOURCE%/*}/../helpers/select-option.sh"

TMUX_SESSION_NAME="syncmd"

tmux has-session -t "${TMUX_SESSION_NAME}" 2>/dev/null

if [ $? = 0 ]; then
    log_error "Tmux session with name '${TMUX_SESSION_NAME}' exists already, kill that session first"
    exit 1
fi

if ! [ -x "$(command -v tmux)" ]; then
    log_error "Tmux is not installed"
    exit 1
fi

if [[ "${1}}" == *$'\n'* ]]; then
    mapfile -t AVAILABLE_DIRECTORIES < <(echo "${1}" | sort)
else
    mapfile -t AVAILABLE_DIRECTORIES < <(find * -maxdepth 0 -type d | grep --ignore-case "${1}" | sort)
fi

if [ ${#AVAILABLE_DIRECTORIES[@]} -eq 0 ]; then
    log_error "No directories to choose from"
    exit 1
fi

select_option DIRECTORIES "In which directories would you like to run commands?" AVAILABLE_DIRECTORIES true

tmux new-session -d -c "$(echo "${PWD}")" -s "${TMUX_SESSION_NAME}"

for DIRECTORY in "${DIRECTORIES[@]}"; do
    tmux split-window -t "${TMUX_SESSION_NAME}:" -c "${PWD}/${DIRECTORY}" "/usr/bin/env bash --login"
    tmux select-layout -t "${TMUX_SESSION_NAME}:" tiled
done

tmux kill-pane -t 1
tmux set-option -t "${TMUX_SESSION_NAME}:" synchronize-panes on

tmux attach -t "${TMUX_SESSION_NAME}"
