#!/usr/bin/env bash

source "${BASH_SOURCE%/*}/../helpers/log.sh"

report="$(find . -type d -name 'node_modules' -prune -o -type d -name '.git' -prune -o -path '*/lcov-report/index.html' -print -quit)"

if [[ -z "${report}" ]]; then
    log_error "No coverage report found"
    exit 1
fi

open "${report}"
