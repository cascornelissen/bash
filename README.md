-----

> **Deprecated ⚰️**  
> This project has been superseded by [cascornelissen/shell](https://gitlab.com/cascornelissen/shell).

-----

# Bash
Aliases, scripts, prompt (PS1), and more for Bash

## Installation
Execute the following command to download and start the installation script, this clones the repository to `~/.bash` and
runs `setup.sh`.

```shell
$ bash <(curl -s https://gitlab.com/cascornelissen/bash/-/raw/master/install.sh)
```

## Setup
The installation script should automatically run the setup script after it has finished installing, but it can also be
executed manually by running the following command.

```shell
$ bash ~/.bash/setup.sh
```

### Prompt (PS1)
Asks if you're interested in updating the shell's prompt (PS1) to the following format. `PS1_USER` and
`PS1_HOST` will be injected by the answers given to these answers.

```
[HH:mm:ss] <PS1_USER>@<PS1_HOST> ~/current/directory (branch)
```

### Aliases
Sets up selected aliases, this list contains a short explanation for each alias.

### Brew and software
Installs [Brew][brew] and additional software based on the selected options.

### Default shell
Sets the default shell to `bash` for both the current and `root` user.

### Software configuration
A few minor default configuration files for software such as `git`, `nano`, and `tmux` are included in this repository.

### Dock organization for macOS
Adds and re-orders software on the macOS dock.

### Automatic updates
This project can automatically check for updates, it will do so in the background when a new shell is started.



[brew]: https://brew.sh/