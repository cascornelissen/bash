#!/usr/bin/env bash

source "${BASH_SOURCE%/*}/../helpers/log.sh"

list_pods() {
    kubectl get pods --field-selector=status.phase==Running --no-headers --output custom-columns=":metadata.labels.app,:metadata.name"
}

find_pod() {
    echo "${1}" | grep -i "^${2} " | awk '{print $2}'
}

forward_port() {
    while read -r line; do
        case "$line" in
            *"Forwarding from [::1]"*)
                log_verbose "${line} → ${1}"
                ;;
            *"Forwarding from"*)
                log_info "${line} → ${1}"
                ;;
            *"Handling connection"*)
                log_debug "${line} → ${1}"
                ;;
            *"address already in use"*)
                log_error "Port ${2} is already in use"
                ;;
            *"Error: No such container"*)
                log_error "Container ${1} no longer available, trying to connect to replacement..."
                reconnect "$@"
                ;;
            *"container not running"*)
                log_error "Container ${1} is not running, trying to connect to replacement..."
                reconnect "$@"
                ;;
            *"error creating error stream"*)
                log_error "Container ${1} returns error when creating error stream, trying to connect to replacement..."
                reconnect "$@"
                ;;
            *"error creating forwarding stream"*)
                log_error "Container ${1} returns error when creating forwarding stream, trying to connect to replacement..."
                reconnect "$@"
                ;;
            *"Error from server (NotFound)"*)
                log_error "Container ${1} no longer available, trying to connect to replacement..."
                reconnect "$@"
                ;;
            *"lost connection to pod"*)
                log_error "Lost connection to pod for container ${1}, trying to connect to replacement..."
                reconnect "$@"
                ;;
            *"unable to listen on any of the requested ports"*)
                ;;
            *)
                echo "${line}"
                ;;
        esac
    done < <(kubectl port-forward "${1}" "${2}" 2>&1)
}

reconnect() {
    sleep 2s

    # Clean up running port-forward
    /bin/bash -c "${BASH_SOURCE%/*}/../miscellaneous/free-port.sh ${2}"

    # Instantiate a new port-forward
    forward_port "$(find_pod "$(list_pods)" "${3}")" "${2}" "${3}"
}

log_info "Connecting to Kubernetes context $(kubectl config current-context)..."

PODS="$(list_pods)"

for argument in "$@"; do {
    IFS=':' read -r -a PARTS <<< "${argument}"
    POD="$(find_pod "${PODS}" "${PARTS[0]}")"
    PORT="${PARTS[1]}"

    if [ -n "${CMD}" ]; then
        CMD+=" & "
    fi

    CMD+="forward_port ${POD} ${PORT} ${PARTS[0]}"
} done

(trap 'kill 0' SIGINT; eval "${CMD}")
