#!/usr/bin/env bash

source "${BASH_SOURCE%/*}/../helpers/select-option.sh"

INPUT="${1}"
mapfile -t options < <(docker ps --format "{{.Names}}" | grep "${INPUT}" | sort)

shift;
CMD="$*"

if [ -z "${CMD}" ]; then
    CMD="/bin/sh"
fi

if [ "${#options[@]}" -eq 0 ]; then
    if [ -n "${INPUT}" ]; then
        echo "No running containers matching '${INPUT}' found"
    else
        echo "No running containers found"
    fi
elif [ "${#options[@]}" -eq 1 ]; then
    docker exec -ti "$(echo "${options[0]}" | cut -d' ' -f1)" "${CMD}"
else
    select_option answer "Which running container would you like to connect to?" options

    docker exec -ti "$(echo "${answer}" | cut -d' ' -f1)" "${CMD}"
fi
