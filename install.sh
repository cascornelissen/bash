#!/usr/bin/env bash

INSTALL_DIR="$(eval echo "~/.bash")"

if ! [ -x "$(command -v git)" ]; then
    echo "Can not install, git is not installed"
    exit
fi

if [[ ! -d "${INSTALL_DIR}" ]]; then
    git clone https://gitlab.com/cascornelissen/bash.git "${INSTALL_DIR}" --quiet
else
    echo "Directory ${INSTALL_DIR} exists already; skipping git clone"
fi

source "${INSTALL_DIR}/setup.sh"
