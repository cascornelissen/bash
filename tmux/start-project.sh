#!/usr/bin/env bash

source "${BASH_SOURCE%/*}/../helpers/log.sh"
source "${BASH_SOURCE%/*}/../helpers/select-option.sh"

mapfile -t options < <(find ~/.config/tmuxinator -name '*.yml' -exec basename {} .yml \; | grep "${1}" | sort)

if [ "${#options[@]}" -eq 0 ]; then
    log_error "No tmux configurations matching '${1}' found"
elif [ "${#options[@]}" -eq 1 ]; then
    tmuxinator start "$(echo "${options[0]}" | cut -d' ' -f1)"
else
    select_option configuration "Which tmux configuration would you like to start?" options

    tmuxinator start "${configuration}"
fi
